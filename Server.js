const http = require('http');
const url = require('url');
const fs = require('fs');
const webSocketServer = require('websocket').server;
const path = require('path');
var childproc = require("child_process");
const prefix = "http://"
const hostname = '0.0.0.0';
const loginport = 3000;
const wsport = 3001;

var clients = [];
var responder = childproc.fork('testparse.js');


var loginserver = http.createServer(function (req, res) {
    var q = url.parse(req.url, true);
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    console.log([req.connection.remoteAddress, url.parse(req.url, true).pathname]);
    if (q.pathname == '/') {
        fs.readFile('./challenge.html', function (err, data) {
            if (err) {
                res.writeHead(404, {
                    'Content-Type': 'text/html'
                });
                return res.end("404 Not Found");
            }
            res.writeHead(200, {
                'Content-Type': 'text/html'
            });
            res.write(data);
            return res.end();
        });
    } else if (q.pathname == '/challenge.js') {
        fs.readFile('./challenge.js', function (err, data) {
            if (err) {
                res.writeHead(404, {
                    'Content-Type': 'text/html'
                });
                return res.end("404 Not Found");
            }
            res.writeHead(200, {
                'Content-Type': 'text/javascript'
            });
            res.write(data);
            return res.end();
        });


    } else {
        console.log(['bad request to login server from ', req.connection.remoteAddress])
    }
}).listen(loginport, hostname, () => {
    console.log('server running at ', hostname + ':', loginport)
});

wssr = http.createServer(function (req, res) {
    console.log('received request');
}).listen(wsport, hostname, () => {
    console.log(`wsServer running at http://${hostname}:${wsport}/`)
});
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket
    // request is just an enhanced HTTP request. For more info 
    // http://tools.ietf.org/html/rfc6455#page-6
    httpServer: wssr
});
var getresponse = function (usrin) {
    console.log(usrin);
    responder.send(usrin);

}

wsServer.on('request', function (request) {
    console.log((new Date()) + ' Connection from origin ' + request.remoteAddress + ' via ' + request.origin);
    // accept connection - you should check 'request.origin' to
    // make sure that client is connecting from your website
    // (http://en.wikipedia.org/wiki/Same_origin_policy)
    var connection = request.accept(null, request.origin);
    // we need to know client index to remove them on 'close' event
    var index = clients.push(connection) - 1;
    console.log(new Date() + ' Connection accepted.' + connection.remoteAddress);
    // send back chat history
    //connection.send("Retrieving current state");
    connection.send(JSON.stringify({
        type: "CHAT",
        msg: "Hello, I'm Kapinski and I'm a robo-receptionist. Welcome to the Adlon Hotel!"
    }));
    connection.send(JSON.stringify({
        type: "CHAT",
        msg: "Are you thinking about making a reservation?"
    }));
    connection.on('message', function (message) {
        console.log("WS:");
        //console.log(connection);
        for (var i in message) {
            console.log(message[i]);
        }


        if (message.type === 'utf8') { // accept only text
            console.log([connection.remoteAddress, JSON.parse(message.utf8Data)]);
            message = JSON.parse(message.utf8Data);
            if (message.type == 'USER') {
                var response = getresponse(message.msg)

            }

        }
    });
    connection.on('close', function (connection) {
        console.log((new Date()) + " Peer " +
            clients[index].remoteAddress + " disconnected.");
        // remove user from the list of connected clients
        clients.splice(index, 1);


    });
});
responder.on('message', (msg) => {

    console.log('Message from child', msg);
    if (msg.indexOf("https:") > -1) {
        clients[0].sendUTF(JSON.stringify({
            type: 'URL',
            msg: msg.slice(6)
        }));
    } else {
        clients[0].sendUTF(JSON.stringify({
            type: 'CHAT',
            msg: msg
        }));
    }
});
