var input = document.getElementById('inputtext');
var submit = document.getElementById('inputbutton');
var frame = document.getElementById('iframe');
var log = document.getElementById('log');
var explainer = document.getElementById('explainer');
var ip = '0.0.0.0';
var ws;
console.log("I think your IP is " + ip);
console.log("If this is wrong, type 'setip(*your IP as a string*)'");
var display = function (msg) {
    log.innerHTML += '\n' + msg + '<br><br>'
}
var seturl = function (url) {
    frame.src = url;

}
var setip = function (ip) {
    var wsConnect = function () {
        var connection = new WebSocket('ws://' + ip + ':3001');
        connection.onopen = function () {
            console.log("ws opened");
            connection.send(JSON.stringify({
                type: "OPEN",
                text: "none"
            }))
        };
        connection.onerror = function (error) {
            console.log("Error connecting to Wss server");
            display("Error connecting to chat server");
            console.log(error)
        };
        connection.onmessage = function (message) {
            msg = JSON.parse(message.data);
            console.log(msg);
            if (msg.type == 'CHAT') {
                display('BOT: ' + msg.msg)
            }
            if (msg.type == 'URL') {
                display('*loading a page for you ->*')
                seturl(msg.msg)
            }
        }



        return connection;
    }
    ws = wsConnect();
    submit.addEventListener('click', function (event) {
        ws.send(JSON.stringify({
            type: 'USER',
            msg: input.value
        }))
        display('ME:  ' + input.value)
        explainer.style = "display:none";
        iframe.style = 'display:inline-block;'; //-webkit-transform:scale(0.5);-moz-transform-scale(0.5);';
    })
}
setip(ip);
