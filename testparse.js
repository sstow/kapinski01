let c = console;
//var msg = "Yes, but I want to ask some questions about the room first: How Big are the rooms and do they have a bath?";
//var msg = "Thank you: how close is the hotel to all the most popular things in berlin?";
//var msg = "can I pay with an american express card?"
//var msg = "when is the earliest time I can check out, my flight is at 6am? and can you organise me a taxi to the airport?"
//var msg = "shrek is love. shrek is life. who's in my swamp? hey donkey! welcome!";
//var msg = "yes. I want to book 5 nights in october"
//var msg = "maybe, can you tell me about the rooms first?"
//var msg = "I don't know, how expensive will a suite be for 4 nights?"
//var msg = "does the restaurant have a vegetarian menu"
//var msg = "what is the architecture like?"
//var msg = "is the berlin wall nearby?"
//var msg = "can i have delivered breakfast to the room"

var cp = require('child_process');
var quoter = cp.fork('reviewsearch.js');
quoter.on('message',function(message){
    say('guests tell me this about '+message[0]+': '+message[1])
})
var msghistory = [];
var keywords = function (words) {
    var blacklist = 'tellhiaskdontbooknowaboutareyesnookyoudoeshavethewheniscanmyourweandtowillifwantatmeandwhathowwhenlikethere';
    var keywords = [];
    //    c.log(words);
    for (var w in words) {
        //        c.log(words[2],)
        if (blacklist.indexOf(words[w]) == -1) {
            keywords.push(words[w])
        }
    }
    return keywords
}
var browseto = function (url) {
    console.log(url)
    process.send(url)
}
var say = function (msg) {
    //c.log('>>>>>>>>>>>>>>>>>>>>>>')
    console.log(msg)
    process.send(msg)
}
var getquote = function (key) {
    c.log('gettting quote for ', key)
    quoter.send(key);
//    quoter = cp.spawn('py', ['./python/reviewsearch.py', key]);
//    quoter.stdout.on('data', function (data) {
//        console.log('got message:')
//        reps = JSON.parse(data)
//        if (reps[0].length<100){
//            reps[0]+=reps[1]
//        }
//        say("Guests say this about "+key+': '+reps[0])
//    })
//    quoter.stderr.on('data', function (data) {
//        console.log('got message:')
//        console.log(JSON.parse(data))
//    })
//    return false
}
var faqsearch = function (words) {
    var scoring = {
        chat: {
            points:0,
            phrases:'hellohihowyoumynameisalice'
        },
        wlan: {
            points: 0,
            phrases: 'wificonnectionwlanconnectivityspeeddslonlineinternetwebwireless'
        },
        checkinout: {
            points: 0,
            phrases: 'earliestlatestimecheckincheckoutreadyarriveleaveearlyopenreceptionhoursdeparturereceptionfinishclose'
        },
        facilities: {
            points: 0,
            phrases: 'barspoolsfacilitiesterracesswimmingspasaunasteam'
        },
        transport: {
            points: 0,
            phrases: 'directionstrainsairportsflightstaxislimousinesparkcarparkingspaceshuttletransportbusescoachesschedulestationsmetroundergroundubahnrentalmotorwayhighway'
        },
        services: {
            points: 0,
            phrases: 'conciergeserviceslanguagesfrenchgermandutchspanishenglishluggageporterbagssuitcasesdrycleaninghelpstaff'

        },
        food: {
            points: 0,
            phrases: 'breakfastrestaurantsmenuglutenmealscuisinelunchdinnercheffoodmealscafesdiningeatingvegetariansveganscoffeeteakettle'
        },
        thehotel: {
            points: 0,
            phrases: 'architecturespaceloungeseatingstairselevatorhotelbuildinghistory'
        },
        rooms: {
            points: 0,
            phrases: 'bigensuitesdoublesingleshowerbathroomssuperiorjuniorluxuriouschildrenchildsbedssofawindowsbalconyapartmentsaccomodateaccomodation'
        },
        booking: {
            points: 0,
            phrases: 'reserveexpensivereservationsbookingsnightsweeksdaystaypeoplecancellationsrebookingoffersratesdiscountsdatesmonthsdaysmondaytuesdaywednesdaytursdayfridayavailabilityjanuaryfebruarymarchaprilmayjunejulyaugustseptembernovemberdecember'
        },
        nearby: {
            points: 0,
            phrases: 'walktouristwherefarnearbycloseattractionssightseeingvisitorcentremuseumsgalleriesattractionsparkspopularthingssee'
        },
        business: {
            points: 0,
            phrases: 'conferencemeetingcentrebusinesscorporatework'
        },
        pets: {
            points: 0,
            phrases: 'dogspetscats'
        },
        payment: {
            points: 0,
            phrases: 'expensiveatmcreditdebitamexamericanexpresscardcashchequepaybillscostpricespay'
        }
    }
    var lastword = '';
    var bestkey = '';
    for (var i in words) {
        var bonus = 0;
        if (lastword == 'the' || lastword == 'a' || lastword == 'how' || lastword == 'an' ||lastword=='to') {
            bonus = 2
        }
        //        console.log(words[i])
        if (words[i].length > 2) {
            for (var j in scoring) {
                if (scoring[j].phrases.indexOf(words[i]) > -1) {
                    bestkey = words[i];
                    scoring[j].points += 1 + bonus;
                }
            }
        }
        lastword = words[i];
    }
    var totalpoints = 0;
    sortable = [];
    for (var j in scoring) {
        totalpoints += scoring[j].points;
        sortable.push([j, scoring[j].points])
        //        console.log(scoring[j], scoring[j].points)
    }
    sortable.sort(function (b, a) {
        return a[1] - b[1];
    });
    return [sortable[0], sortable[1], sortable[2], totalpoints, bestkey]
}
//create a copy of the msg

var cobrowse = function (topic, keys) {
    c.log('TOIPIC', topic)
    key = keys[0] //topic[4];
    c.log('KEY:', key);
    switch (topic[0][0]) {
        case 'chat':
            say('hello, i am fine')
            break
        case 'facilities':
            say('have a look at some of the facilites we offer');
            getquote(key);
            if ('spa' in keys) {
                browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany/services-amenities/spa')
            } else {
                browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany')
            }
            break
        case 'thehotel':
            say('have a look at some of the information about that here:');
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany');
            getquote(key);
            break
        case 'checkinout':
            say('checkin and checkout are both available 24hrs, stress free');
            getquote(key);
            break
        case 'transport':
            say('reception is happy to help with all of your transportation needs');
            say('we also offer a limousine service')
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany');
            getquote(key);
            break
        case 'food':
            say('there are many great restaurants providing for all tastes in and near the Adlon');
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany/services-amenities/dining')
            getquote(key);
            break
        case 'wlan':
            say('the hotel has great wifi');
            getquote(key);
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany');
            break
        case 'services':
            say('there are many great services available including concierge');
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany');
            getquote(key);
            break
        case 'business':
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany/event-venues');
            getquote(key);
            break
        case 'payment':
            say('we have no problem accepting all payment types');
            break
        case 'pets':
            say('well behaved animals are permitted');
            getquote(key);
            break
        case 'nearby':
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany');
            getquote(key)
            break
        case 'rooms':
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany/rooms');
            say('here is some more information about our rooms');
            getquote(key);
            break
        case 'booking':
            say('take a look at our offers and make a booking here!')
            browseto('https://www.lhw.com/hotel/Hotel-Adlon-Kempinski-Berlin-Germany/offers')
            break
        getquote(key)

    }
}
//do some lazy sentence parsing
//if they write the date as '19. 8. 2019' or a name as 'mrs. Doubtfire' or something else this will fail
//also, parse for question marks
//while msg still has '. ' 

var sentence = {
    sentence: '',
    type: '',
    words: []
};
var word = '';
var sentences = [];
var msg;
process.on('message', function (msg) {
    c.log('message: ' + msg);
    msg = msg.toLowerCase();
    var fullmsg = msg.slice(0);
    for (var i in msg) {
        if (msg[i] == '-') {

        } else if (msg[i] == ' ' || msg[i] == ',') {
            if (word.length > 0) {
                sentence.words.push(word);
            }
            word = '';
            sentence.sentence += msg[i];
        } else if (msg[i] == '.' || msg[i] == '?' || msg[i] == '!' || i == msg.length-1) {
            sentence.words.push(word);
            word = '';
            sentence.sentence += msg[i];
            sentences.push(Object.assign({}, sentence));
            topic = faqsearch(sentence.words);
            c.log('she wants to know about:', topic);
            //        if (topic[0][0]=='food'){
            keys = keywords(sentence.words);
            c.log(keys);
            cobrowse(topic, keys);
            //        }
            sentence.sentence = '';
            sentence.words = [];
        } else {
            sentence.sentence += msg[i];
            word += msg[i];
        }
    }
})
console.log(sentences);
//Keep track of message history, to avoid repetition and help remember context
msghistory.push({
    speaker: 'client',
    msg: msg,
    sentences: sentences
})


c.log(msghistory);
//setTimeout(function () {
//    console.log('quitting')
//}, 10000)
