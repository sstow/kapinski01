to make this work:
I did: 
clone the repository 

then:
$ docker build -t kapinski01 . 

You need to map the ports as 3000:3000 and 3001:3001
so that's 
$ docker run -p 3000:3000 -p 3001:3001 kapinski01

I have a lot of difficulty with the IP addresses because it works very badly with windows 7
hopefully they will work normally on your end.

you will need to go to the correct ip:3000.
if your ip is different to the one the client expects, you will be prompted and can enter it in the console using:
> getip('127.0.0.1')
or whatever your ip is. this is used by the websocket inside the client.

