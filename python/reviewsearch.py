# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 12:38:02 2018

@author: Default User
"""
import sys
import numpy as np
import pandas as pd
import json
#python review searcher
vocabulary = pd.read_csv('D:/vocabularylist.csv',sep='\n')#.values
vocab=[]
sentences = pd.read_csv('D:/singlesentences.csv',sep='\n')
searchword = sys.argv[1]
for i in vocabulary:
    vocab.append(i[0])
if len([w[0] for w in vocabulary.values if searchword in w[0]])>0:
    output = [s[0] for s in sentences.values if searchword in s[0]]
    if len(output)>10:
        sys.stdout.write(json.dumps(output[0:3]))
    else:
        sys.stdout.write(json.dumps(output[0:3]))
else:
    sys.stdout.write("Sorry, I don't know anything about " + searchword)
quit()